<?php

namespace Http;

use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

class MethodTest extends TestCase
{
    public function test__construct()
    {
        $method = new Method(Method::GET);
        $this->assertEquals(Method::GET, $method->getValue());

        $this->expectException(InvalidArgumentException::class);
        $method = new Method('wrong value');
    }

    public function testGetValue()
    {
        $method = new Method(Method::PUT);
        $this->assertEquals(Method::PUT, $method->getValue());
    }

    public function testSetValue()
    {
        $method = new Method(Method::COPY);
        $fluent = $method->setValue(Method::POST);
        $this->assertSame($method, $fluent);
        $this->assertEquals(Method::POST, $method->getValue());
    }

    public function testHasBody()
    {
        $method = new Method(Method::GET);
        $this->assertFalse($method->hasBody());

        $method->setValue(Method::POST);
        $this->assertTrue($method->hasBody());
    }

    public function testHasOptionalBody()
    {
        $method = new Method(Method::POST);
        $this->assertFalse($method->hasOptionalBody());

        $method->setValue(Method::OPTIONS);
        $this->assertTrue($method->hasOptionalBody());

        $method->setValue(Method::GET);
        $this->assertFalse($method->hasOptionalBody());
    }

    public function testCanHandleBody()
    {
        $method = new Method(Method::GET);
        $this->assertFalse($method->canHandleBody());

        $method->setValue(Method::POST);
        $this->assertTrue($method->canHandleBody());

        $method->setValue(Method::OPTIONS);
        $this->assertTrue($method->canHandleBody());
    }

    public function test__toString()
    {
        $method = new Method(Method::GET);
        $this->assertEquals(Method::GET, $method->__toString());
    }
}
