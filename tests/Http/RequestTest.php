<?php

namespace Http;

use PHPUnit\Framework\TestCase;
use ResourceClass\Curl\Curl;
use Uri\Uri;

class RequestTest extends TestCase
{
    public function test__construct()
    {
        $request = new Request();

        $this->assertEquals([], $request->getHeaderParameters());
        $this->assertEquals([], $request->getQueryParameters());
    }

    public function testGetUri()
    {
        $request = new Request();
        $this->assertNull($request->getUri());

        $uri = 'http://example.com';
        $request->setUri($uri);
        $this->assertEquals($uri, $request->getUri()->__toString());
    }

    public function testSetUri()
    {
        $request = new Request();

        $uri = 'http://example.com';
        $fluent = $request->setUri($uri);

        $this->assertSame($fluent, $request);
        $this->assertEquals($uri, $request->getUri()->__toString());

        $parsedUri = Uri::parse($uri);
        $request->setUri($parsedUri);
        $this->assertSame($parsedUri, $request->getUri());

        $this->expectException(\InvalidArgumentException::class);
        $request->setUri(544);
        $request->setUri('http:/malformed.com');
    }

    public function testGetMethod()
    {
        $request = new Request();
        $this->assertNull($request->getMethod());

        $request->setMethod(Method::GET);
        $this->assertEquals(Method::GET, $request->getMethod()->getValue());
    }

    public function testSetMethod()
    {
        $request = new Request();

        $fluent = $request->setMethod(Method::GET);
        $this->assertSame($fluent, $request);
        $this->assertEquals(Method::GET, $request->getMethod()->getValue());

        $method = new Method(Method::POST);
        $request->setMethod($method);
        $this->assertSame($method, $request->getMethod());

        $this->expectException(\InvalidArgumentException::class);
        $request->setMethod('not a method');
    }

    public function testGetHeaderParameters()
    {
        $request = new Request();
        $this->assertEquals([], $request->getHeaderParameters());

        $headers = ['Content-Type' => 'application/json'];
        $request->setHeaderParameters($headers);
        $this->assertEquals($headers, $request->getHeaderParameters());
    }

    public function testSetHeaderParameters()
    {
        $request = new Request();

        $headers = ['Content-Type' => 'application/json'];
        $fluent = $request->setHeaderParameters($headers);
        $this->assertSame($fluent, $request);
        $this->assertEquals($headers, $request->getHeaderParameters());

        $this->expectException(\InvalidArgumentException::class);
        $request->setHeaderParameters(['Not-a-pascalcamelCase', 'just value']);
    }

    public function testSetHeaderParameter()
    {
        $request = new Request();

        $fluent = $request->setHeaderParameter('Content-Type', 'application/json');
        $this->assertSame($fluent, $request);
        $this->assertEquals(['Content-Type' => 'application/json'], $request->getHeaderParameters());

        $this->expectException(\InvalidArgumentException::class);
        $request->setHeaderParameter('Not-a-pascalcamelCase', 'just value');
    }

    public function testGetQueryParameters()
    {
        $request = new Request();
        $this->assertEquals([], $request->getQueryParameters());

        $queryParameters = ['value' => 'some value'];
        $request->setQueryParameters($queryParameters);
        $this->assertEquals($queryParameters, $request->getQueryParameters());
    }

    public function testSetQueryParameters()
    {
        $request = new Request();

        $queryParameters = ['value' => 'some value'];
        $fluent = $request->setQueryParameters($queryParameters);
        $this->assertSame($fluent, $request);
        $this->assertEquals($queryParameters, $request->getQueryParameters());
    }

    public function testSetQueryParameter()
    {
        $request = new Request();

        $fluent = $request->setQueryParameter('value', 'some value');
        $this->assertSame($fluent, $request);
        $this->assertEquals(['value' => 'some value'], $request->getQueryParameters());
    }

    public function testGetBodyParameters()
    {
        $request = new Request();
        $this->assertEquals([], $request->getBodyParameters());

        $bodyParameters = ['value' => 'some value'];
        $request->setBodyParameters($bodyParameters);
        $this->assertEquals($bodyParameters, $request->getBodyParameters());
    }

    public function testSetBodyParameters()
    {
        $request = new Request();
        $request->setBody('body to erase');

        $bodyParameters = ['value' => 'some value'];
        $fluent = $request->setBodyParameters($bodyParameters);
        $this->assertSame($fluent, $request);
        $this->assertEquals($bodyParameters, $request->getBodyParameters());
        $this->assertNull($request->getBody());
    }

    public function testSetBodyParameter()
    {
        $request = new Request();
        $request->setBody('body to erase');

        $fluent = $request->setBodyParameter('value', 'some value');
        $this->assertSame($fluent, $request);
        $this->assertEquals(['value' => 'some value'], $request->getBodyParameters());
        $this->assertNull($request->getBody());
    }

    public function testGetBody()
    {
        $request = new Request();
        $this->assertNull($request->getBody());

        $body = 'raw body';
        $request->setBody($body);
        $this->assertEquals($body, $request->getBody());
    }

    public function testSetBody()
    {
        $request = new Request();
        $request->setBodyParameter('Content-Type', 'application/json');

        $body = 'raw body';
        $fluent = $request->setBody($body);
        $this->assertSame($fluent, $request);
        $this->assertEquals($body, $request->getBody());
        $this->assertEquals([], $request->getBodyParameters());
    }

    public function testGetCurl()
    {
        $request = $this->prepareRequest();
        $this->assertNull($request->getCurl());

        $request->prepareCurl();
        $this->assertInstanceOf(Curl::class, $request->getCurl());
    }

    public function testSetCurl()
    {
        $request = new Request();
        $curl = Curl::init();

        $fluent = $request->setCurl($curl);
        $this->assertSame($fluent, $request);
        $this->assertInstanceOf(Curl::class, $request->getCurl());
    }

    public function testGetResponse()
    {
        $request = $this->prepareRequest();
        $this->assertNull($request->getResponse());

        $request->exec();
        $this->assertInstanceOf(Response::class, $request->getResponse());
    }

    public function testSetResponse()
    {
        $request = new Request();

        $response = new Response();
        $fluent = $request->setResponse($response);
        $this->assertSame($fluent, $request);
        $this->assertSame($response, $request->getResponse());
    }

    public function testExec()
    {
        $request = $this->prepareRequest();

        $request->exec();
        $this->assertInstanceOf(Curl::class, $request->getCurl());
        $this->assertInstanceOf(Response::class, $request->getResponse());

        $response = $request->getResponse();
        $json = <<<'JSON'
{
  "userId": 1,
  "id": 1,
  "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
}
JSON;

        $this->assertEquals(StatusCode::OK, $response->getHttpStatus());
        $this->assertStringContainsString('application/json', $response->getHeaders()['Content-Type'] ?? null);
        $this->assertEquals($json, $response->getBody());
        $obj = json_decode($json);
        $this->assertEquals($obj, $response->getParsedBody());
    }

    public function testPrepareCurl()
    {
        $request = $this->prepareRequest();
        $curl = $request->prepareCurl();

        $this->assertInstanceOf(Curl::class, $request->getCurl());
        $this->assertSame($curl, $request->getCurl());
    }

    /**
     * Prepare a request for some tests.
     * @return Request The created request
     */
    private function prepareRequest(): Request
    {
        $request = new Request();
        $request
            ->setMethod(Method::GET)
            ->setUri('https://jsonplaceholder.typicode.com/posts/1')
        ;

        return $request;
    }
}
