<?php

namespace Http;

use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    public function test__construct()
    {
        $response = new Response();
        $this->assertEquals([], $response->getHeaders());
        $this->assertNull($response->getHttpStatus());
        $this->assertNull($response->getBody());
        $this->assertNull($response->getParsedBody());
    }

    public function testGetHttpStatus()
    {
        $response = new Response();
        $this->assertNull($response->getHttpStatus());

        $response->setHttpStatus(StatusCode::ACCEPTED);
        $this->assertEquals(StatusCode::ACCEPTED, $response->getHttpStatus());
    }

    public function testSetHttpStatus()
    {
        $response = new Response();

        $fluent = $response->setHttpStatus(StatusCode::ALREADY_REPORTED);
        $this->assertSame($response, $fluent);
        $this->assertEquals(StatusCode::ALREADY_REPORTED, $response->getHttpStatus());
    }

    public function testGetHeaders()
    {
        $response = new Response();
        $this->assertEquals([], $response->getHeaders());

        $headers = ['Content-Type' => 'text/html'];
        $response->setHeaders($headers);
        $this->assertEquals($headers, $response->getHeaders());
    }

    public function testSetHeaders()
    {
        $response = new Response();

        $headers = ['Content-Type' => 'text/html'];
        $fluent = $response->setHeaders($headers);
        $this->assertSame($response, $fluent);
        $this->assertEquals($headers, $response->getHeaders());
    }

    public function testGetBody()
    {
        $response = new Response();
        $this->assertNull($response->getBody());

        $body = 'random body';
        $response->setBody($body);
        $this->assertEquals($body, $response->getBody());
    }

    public function testSetBody()
    {
        $response = new Response();

        $obj = (object) [
            'ex' => 5,
            'efd' => null,
        ];
        $body = json_encode($obj);

        // enables the parse of the JSON body
        $response->setHeaders(['Content-Type' => 'application/json']);

        $fluent = $response->setBody($body);
        $this->assertSame($fluent, $response);

        $this->assertEquals($body, $response->getBody());
        $this->assertEquals($obj, $response->getParsedBody());

        $body = <<<'HTML'
<!DOCTYPE html>
<html>
<body>
<p>A random paragraph</p>
</body>
</html>
HTML;

        $response->setHeaders(['Content-Type' => 'text/html']);
        $response->setBody($body);
        $this->assertEquals($body, $response->getBody());
        $this->assertInstanceOf(\DOMDocument::class, $response->getParsedBody());
    }

    public function testGetParsedBody()
    {
        $response = new Response();
        $this->assertNull($response->getParsedBody());

        $obj = (object) [
            'new' => [],
            'prop' => 'random string',
        ];
        $body = json_encode($obj);

        $response->setHeaders(['Content-Type' => 'application/json']);
        $response->setBody($body);

        $this->assertEquals($obj, $response->getParsedBody());
    }
}
