<?php

namespace Http;

use InvalidArgumentException;

/**
 * Class Method contains HTTP method in constants.
 * In some extents, an instance give some extra information, allowing to manipulate an HTTP request depending on
 * the method.
 * @package Http
 */
class Method
{
    public const GET = 'GET';
    public const HEAD = 'HEAD';
    public const POST = 'POST';
    public const PUT = 'PUT';
    public const DELETE = 'DELETE';
    public const OPTIONS = 'OPTIONS';
    public const TRACE = 'TRACE';
    public const CONNECT = 'CONNECT';
    public const PROPFIND = 'PROPFIND';
    public const MKCOL = 'MKCOL';
    public const COPY = 'COPY';
    public const MOVE = 'MOVE';
    public const LOCK = 'LOCK';
    public const UNLOCK = 'UNLOCK';
    public const VERSION_CONTROL = 'VERSION-CONTROL';
    public const REPORT = 'REPORT';
    public const CHECKOUT = 'CHECKOUT';
    public const CHECKIN = 'CHECKIN';
    public const UNCHECKOUT = 'UNCHECKOUT';
    public const MKWORKSPACE = 'MKWORKSPACE';
    public const UPDATE = 'UPDATE';
    public const LABEL = 'LABEL';
    public const MERGE = 'MERGE';
    public const BASELINE_CONTROL = 'BASELINE-CONTROL';
    public const MKACTIVITY = 'MKACTIVITY';
    public const ORDERPATCH = 'ORDERPATCH';
    public const ACL = 'ACL';
    public const PATCH = 'PATCH';

    private const AVAILABLE_METHODS = [
        self::GET,
        self::HEAD,
        self::POST,
        self::PUT,
        self::DELETE,
        self::OPTIONS,
        self::TRACE,
        self::CONNECT,
        self::PROPFIND,
        self::MKCOL,
        self::COPY,
        self::MOVE,
        self::LOCK,
        self::UNLOCK,
        self::VERSION_CONTROL,
        self::REPORT,
        self::CHECKOUT,
        self::CHECKIN,
        self::UNCHECKOUT,
        self::MKWORKSPACE,
        self::UPDATE,
        self::LABEL,
        self::MERGE,
        self::BASELINE_CONTROL,
        self::MKACTIVITY,
        self::ORDERPATCH,
        self::ACL,
        self::PATCH,
    ];

    /**
     * Contains the method name.
     * @var string $value
     */
    private $value;

    /**
     * HttpMethod constructor.
     * @param string $value The HTTP method, which must be one of the {@see Method} constant.
     */
    public function __construct(string $value)
    {
        $this->setValue($value);
    }

    /**
     * Get the {@see Method::$value}.
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the {@see Method::$value}. Must be one of {@see Method} constant.
     * @param string $value The new value to set.
     * @return $this
     */
    public function setValue(string $value): self
    {
        if (!in_array($value, self::AVAILABLE_METHODS, true)) {
            throw new InvalidArgumentException('No method "' . $value . '" available.');
        }

        $this->value = $value;
        return $this;
    }

    /**
     * Checks if the method requires a body.
     * @return bool true if the HTTP method needs a body, false otherwise.
     */
    public function hasBody()
    {
        switch ($this->getValue()) {
            case self::POST:
            case self::PUT:
            case self::CONNECT:
            case self::PATCH:
                return true;
            default:
                return false;
        }
    }

    /**
     * Checks if a could have an optional body.
     * If a method must have a body, this function will return false.
     * @return bool True if the HTTP method could have a body, false otherwise.
     */
    public function hasOptionalBody()
    {
        switch ($this->getValue()) {
            case self::OPTIONS:
                return true;
            default:
                return false;
        }
    }

    /**
     * Checks if the given method is able to handle a body inside.
     * Concatenate {@see Method::hasBody()} and {@see Method::hasOptionalBody()} with a logical OR.
     * @return bool True if the method can handle a body, false otherwise.
     */
    public function canHandleBody()
    {
        return $this->hasBody() || $this->hasOptionalBody();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getValue();
    }
}