<?php

namespace Http;

use DOMDocument;
use Http\Exception\ExtensionNotLoadedException;
use stdClass;

/**
 * Class Response store the result of a request.
 * @package Http
 */
class Response
{
    /**
     * The HTTP status code.
     *
     * @var int $httpStatus
     */
    private $httpStatus;

    /**
     * The response headers.
     *
     * @var string[] $headers
     */
    private $headers;

    /**
     * The response body.
     *
     * @var string[]|string $body
     */
    private $body;

    /**
     * The parsed response body, if available.
     *
     * @var mixed|DOMDocument|stdClass|null $parsedBody
     */
    private $parsedBody;

    /**
     * HttpResponse constructor.
     */
    public function __construct()
    {
        $this->setHeaders([]);
    }

    /**
     * Get the {@see Response::$httpStatus}.
     *
     * @return int
     */
    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    /**
     * Set the {@see Response::$httpStatus}.
     *
     * @param int $httpStatus
     * @return $this
     */
    public function setHttpStatus(int $httpStatus): self
    {
        $this->httpStatus = $httpStatus;

        return $this;
    }

    /**
     * Get the {@see Response::$headers}.
     *
     * @return string[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Set the {@see Response::$headers}.
     *
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get the {@see Response::$body}.
     *
     * @return string|string[]
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set the {@see Response::$body}.
     *
     * @param $body
     * @return $this
     */
    public function setBody($body): self
    {
        $this->body = $body;

        try {
            $this->parseBody();
        } catch (ExtensionNotLoadedException $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
        }

        return $this;
    }

    /**
     * Get the {@see Response::$parsedBody}.
     *
     * @return DOMDocument|mixed|null|stdClass
     */
    public function getParsedBody()
    {
        return $this->parsedBody;
    }

    /**
     * Set the {@see Response::$parsedBody}.
     *
     * @param mixed $parsedBody
     * @return $this
     */
    protected function setParsedBody($parsedBody): self
    {
        $this->parsedBody = $parsedBody;

        return $this;
    }

    /**
     * Try to parse the body depending on the Content-Type header.
     *
     * @return bool
     */
    protected function parseBody(): bool
    {
        $contentType = $this->headers['Content-Type'] ?? null;
        $jsonExtension = extension_loaded('json');
        $domExtension = extension_loaded('dom');

        if ($contentType === null) {
            return false;
        } elseif (strpos($contentType, 'application/json') === 0) {
            if (!$jsonExtension) {
                throw new ExtensionNotLoadedException('The JSON extension is not loaded. Unable to parse the JSON response.');
            }

            $this->setParsedBody(json_decode($this->body));

            return true;
        } elseif (strpos($contentType, 'text/html') === 0) {
            if (!$domExtension) {
                throw new ExtensionNotLoadedException('The DOM extension not loaded. Unable to parse the HTML response.');
            }

            $domDocument = new DOMDocument();

            if (@$domDocument->loadHTML($this->getBody())) {
                $this->setParsedBody($domDocument);

                return true;
            }
        } elseif (strpos($contentType, 'text/xml') === 0 || strpos($contentType, 'application/xml') === 0) {
            if (!$domExtension) {
                throw new ExtensionNotLoadedException('The DOM extension is not loaded. Unable to parse the XML response.');
            }

            $domDocument = new DOMDocument();

            if (@$domDocument->loadXML($this->getBody())) {
                $this->setParsedBody($domDocument);
                return true;
            }
        }

        return false;
    }
}
