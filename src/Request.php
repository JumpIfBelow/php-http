<?php

namespace Http;

use InvalidArgumentException;
use ResourceClass\Curl\Curl;
use Uri\Exception\BadQueryException;
use Uri\Exception\BadSchemeException;
use Uri\Exception\NoHostException;
use Uri\Exception\UriException;
use Uri\Uri;

/**
 * Class Request handling a Curl object containing a cURL resource. Made to easily create an HTTP request,
 * but with a way to custom it as much as needed. All of the internal storage is available to make it useable
 * outside the main purpose if this class.
 *
 * @package Http
 */
class Request
{
    /**
     * The URI of the request.
     *
     * @var Uri $uri
     */
    private $uri;

    /**
     * The method of the request.
     *
     * @var Method $method
     */
    private $method;

    /**
     * An array containing all headers.
     *
     * @var string[] $headerParameters
     */
    private $headerParameters;

    /**
     * An array containing all query parameters.
     *
     * @var string[] $queryParameters
     */
    private $queryParameters;

    /**
     * An array containing all body parameters.
     * Note that using this is excluding the use of {@see Request::$body}.
     *
     * @var string[]|null $bodyParameters
     */
    private $bodyParameters;

    /**
     * The content of the body, in plain text.
     * Note that using this is excluding the use of {@see Request::$bodyParameters}.
     *
     * @var string|null $body
     */
    private $body;

    /**
     * The Curl object containing the request to make.
     * The object itself contains the plain PHP resource.
     *
     * @var Curl $curl
     */
    private $curl;

    /**
     * The response once the request is executed.
     *
     * @var Response $response
     */
    private $response;

    /**
     * HttpRequest constructor.
     */
    public function __construct()
    {
        $this
            ->setHeaderParameters([])
            ->setQueryParameters([])
            ->setBodyParameters([])
        ;
    }

    /**
     * Get the {@see Request::$uri}.
     *
     * @return Uri|null
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set the {@see Request::$uri}.
     *
     * @param Uri|string $uri The URI to use. Could be a string, which will be parsed to an Uri object.
     * @return $this
     */
    public function setUri($uri): self
    {
        $throwsInvalid = false;
        $previous = null;
        if (is_string($uri)) {
            try {
                $this->uri = Uri::parse($uri);
            } catch (UriException $e) {
                $throwsInvalid = true;
                $previous = $e;
            }
        } elseif ($uri instanceof Uri) {
            $this->uri = $uri;
        } else {
            $throwsInvalid = true;
        }

        if ($throwsInvalid) {
            throw new InvalidArgumentException(
                'The $uri parameter must be a valid URI string or an '
                . Uri::class
                . ' object.',
                0,
                $previous
            );
        }

        return $this;
    }

    /**
     * Get the {@see Request::$method}.
     *
     * @return Method|null
     */
    public function getMethod(): ?Method
    {
        return $this->method;
    }

    /**
     * Set the {@see Request::$method}.
     *
     * @param Method|string $method The method to use. Could be a string, which will be parsed to a Method object.
     * @return $this
     */
    public function setMethod($method): self
    {
        if (is_string($method)) {
            $this->method = new Method($method);
        } elseif ($method instanceof Method) {
            $this->method = $method;
        } else {
            throw new InvalidArgumentException('$method must be either a string or a ' . Method::class . ' instance.');
        }

        return $this;
    }

    /**
     * Get the {@see Request::$headerParameters}.
     *
     * @return string[]
     */
    public function getHeaderParameters(): array
    {
        return $this->headerParameters;
    }

    /**
     * Set the {@see Request::$headerParameters}.
     * Note that using this will erase all the headers, even if it is not overriden by the provided array.
     *
     * @param string[] $headerParameters An array of string, where the key is the header name and the value the
     * header value. The header name must in Pascal-Kebab-Case.
     * @return $this
     */
    public function setHeaderParameters(array $headerParameters): self
    {
        $this->headerParameters = [];

        foreach ($headerParameters as $key => $value) {
            $this->setHeaderParameter($key, $value);
        }

        return $this;
    }

    /**
     * Set a new header, without erasing the others. If the header already exists, it will be overriden silently.
     *
     * @param string $key The header name to set. Must be in Pascal-Kebab-Case.
     * @param string $value The header value to set.
     * @return $this
     */
    public function setHeaderParameter(string $key, string $value): self
    {
        if (!preg_match('#^[A-Z][a-z]*(?:-[A-Z][a-z]*)*$#', $key)) {
            throw new InvalidArgumentException('The $key must be in Pascal-Kebab-Case.');
        }

        $this->headerParameters[$key] = $value;
        return $this;
    }

    /**
     * Get the {@see Request::$queryParameters}.
     *
     * @return string[]
     */
    public function getQueryParameters(): array
    {
        return $this->queryParameters;
    }

    /**
     * Set the {@see Request::$queryParameters}.
     * Note that using this will erase all the query parameters, even if it is not overriden by the provided array.
     *
     * @param string[] $queryParameters An array of string, where the key is the query parameter name
     * and the value the query parameter value.
     *
     * @return $this
     */
    public function setQueryParameters(array $queryParameters): self
    {
        $this->queryParameters = [];

        foreach ($queryParameters as $key => $value) {
            $this->setQueryParameter($key, $value);
        }

        return $this;
    }

    /**
     * Set a new query parameter, without erasing the others.
     * If the query parameter already exists, it will be overriden silently.
     *
     * @param string $key The query parameter name to set.
     * @param string $value The query parameter value to set.
     * @return $this
     */
    public function setQueryParameter(string $key, string $value): self
    {
        $this->queryParameters[$key] = $value;

        return $this;
    }

    /**
     * Get the {@see Request::$bodyParameters}.
     *
     * @return string[]
     */
    public function getBodyParameters(): array
    {
        return $this->bodyParameters;
    }

    /**
     * Set the {@see Request::$bodyParameters}.
     * Note that using this will erase all the body parameters, even if it is not overridden by the provided array.
     * In extension, all content in {@see Request::$body}.
     *
     * @param string[] $bodyParameters An array of string, where the key is the query parameter name
     * and the value the query parameter value.
     *
     * @return $this
     */
    public function setBodyParameters(array $bodyParameters): self
    {
        $this->bodyParameters = [];

        foreach ($bodyParameters as $key => $value) {
            $this->setBodyParameter($key, $value);
        }

        return $this;
    }

    /**
     * Set a new body parameter, without erasing the others.
     * If the body parameter already exists, it will be overridden silently.
     * Note that using this method will erase all content in {@see Request::$body}.
     *
     * @param string $key The query parameter name to set.
     * @param string $value The query parameter value to set.
     * @return $this
     */
    public function setBodyParameter(string $key, string $value): self
    {
        $this->bodyParameters[$key] = $value;
        $this->body = null;

        return $this;
    }

    /**
     * Get the {@see Request::$body}.
     *
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * Set the {@see Request::$body}.
     * Note that using this method will erase all content in {@see Request::$bodyParameters}.
     *
     * @param null|string $body
     * @return $this
     */
    public function setBody(?string $body): self
    {
        $this->body = $body;
        $this->bodyParameters = [];

        return $this;
    }

    /**
     * Get the {@see Request::$curl}.
     *
     * @return Curl|null
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * Set the {@see Request::$curl}.
     *
     * @param Curl $curl
     * @return $this
     */
    public function setCurl(Curl $curl): self
    {
        $this->curl = $curl;

        return $this;
    }

    /**
     * Get the {@see Request::$response}.
     *
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set the {@see Request::$response).
     *
     * @param Response|null $response
     * @return $this
     */
    public function setResponse(Response $response = null): self
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Execute the request.
     *
     * @param bool $prepareCurl True if the query needs to be prepared or not.
     * Use it when you have prepared your own cURL logic, wether with {@see Request::prepareCurl()} or not.
     * @return void
     * @throws BadQueryException
     * @throws BadSchemeException
     * @throws NoHostException
     */
    public function exec($prepareCurl = true): void
    {
        if ($prepareCurl) {
            $this->prepareCurl();
        }

        $return = $this->curl->exec();
        $this->setResponse($this->parseResponse($return));
        $this->curl->close();
    }

    /**
     * Prepare the cURL resource with the actual state of the Request object.
     *
     * @return Curl The prepared Curl request, containing URI, headers, query parameters and body parameters/raw body.
     * @throws BadQueryException
     * @throws BadSchemeException
     * @throws NoHostException
     */
    public function prepareCurl(): Curl
    {
        $this->curl = Curl::init($this->buildQuery());
        $this->curl->setOpt(CURLOPT_CUSTOMREQUEST, $this->method->getValue());

        $headers = [];
        foreach ($this->headerParameters as $key => $value) {
            $headers[] = $key . ': ' . $value;
        }

        $this->curl->setOpt(CURLOPT_HTTPHEADER, $headers);

        if ($this->method->canHandleBody()) {
            $this->curl->setOpt(CURLOPT_POST, true);
            $this->curl->setOpt(CURLOPT_POSTFIELDS, $this->body ?: http_build_query($this->bodyParameters));
        }

        $this->curl->setOpt(CURLOPT_RETURNTRANSFER, true);
        $this->curl->setOpt(CURLOPT_HEADER, true);

        return $this->curl;
    }

    /**
     * Build the query string.
     *
     * @return string
     * @throws NoHostException
     * @throws BadQueryException
     * @throws BadSchemeException
     */
    protected function buildQuery(): string
    {
        if ($this->queryParameters !== []) {
            $this
                ->uri
                ->setQuery('?' . http_build_query($this->queryParameters))
            ;
        }

        return $this->uri->__toString();
    }

    /**
     * Parse the response of the request to store it in an ordered object.
     *
     * @param string|false $return The request raw response.
     * @return Response|null The parsed response.
     */
    protected function parseResponse($return)
    {
        if ($return === false || !is_string($return)) {
            return null;
        }

        $headerSize = $this->curl->getInfo(CURLINFO_HEADER_SIZE);
        $stringHeaders = substr($return, 0, $headerSize);

        $arrayHeaders = explode(PHP_EOL, $stringHeaders);
        $arrayHeaders = array_slice($arrayHeaders, 1, -1);

        $body = substr($return, $headerSize);

        $headers = [];
        foreach ($arrayHeaders as $line) {
            if (strpos($line, ': ') === false) {
                continue;
            }

            [$newKey, $newValue] = explode(': ', $line);
            $headers[$newKey] = substr($newValue, 0, -1);
        }

        $response = new Response();
        $response
            ->setHttpStatus($this->curl->getInfo(CURLINFO_HTTP_CODE))
            ->setHeaders($headers)
            ->setBody($body)
        ;

        return $response;
    }
}
