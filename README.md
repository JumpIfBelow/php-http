# PHP HTTP Request
HTTP Request is created to use cURL easily. It can handle all kind of request.
Moreover, if the functionalities does not fit your needs, you can directly get the cURL
class/resource before it is ran, and run it after doing your stuff.


## Installation
Runs the Composer command
```bash
composer require jumpifbelow/php-http
```

## Example
```php
<?php

use Http\Request;
use Uri\Uri;
use Http\Method;

$request = new Request();
$uri = new Uri();

// prepare the URI
$uri
    ->setScheme('https')
    ->setHost('api.example.com')
    ->setPath('/user')
;

// it could also be done like this:
$uri = Uri::parse('https://api.example.com/user');

// prepare the request
$request
    ->setMethod(Method::GET)
    ->setUri($uri)
    // or even faster, you can pass it as string
    ->setUri('https://api.example.com/user')
    ->setQueryParameters([
        'id' => 123456,
    ])
;

$request->exec();
$response = $request->getResponse();

// you can get the raw response or the parsed one, if supported
$bodyResponse = $response->getParsedBody() ?: $response->getBody();

// if you want to set a body, you can do it raw or using parameters
$request
    ->setBodyParameters([
        'param' => 'val',
    ])
    ->setBody('raw body')
;

// note that using bodyParameters or body is exclusive. Using one erases the other
// you may set the content type manually to make everything working
$request->setHeaderParameter('Content-Type', 'text/plain');
```

## Disclaimer
As this package is still in development (and in early stage), not all of the features are well tested.
There is no warranty this package will work, nor it will fit your requirements. You use this
software at your own risks.
